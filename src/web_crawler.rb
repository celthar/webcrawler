# (Sascha Riedl, 17.05.2013)

=begin
Aufruf:
    - ruby ./web_crawler.rb http://www.example.com/
    - Addresse muss auf / enden

Ausgabe:
    Während Programm läuft:
        - Alle loadHtml()-Aufrufe, die einen gültigen "text/html"-Header liefern
        - Alle processHtml()-Aufrufe
    Abschluss/Abbruch des Programmlaufs:
        - Anzahl der unfertigen Jobs
        - Anzahl der Jobs, die in Arbeit sind
        - Anzahl der fertigen Jobs
        - Statistiken zu allen fertigen Jobs
=end

require 'net/http'

# Klasse zum Verwalten der HTML-Daten
class HtmlData
    include Comparable
    def initialize(url)
        @url = URI.parse(URI.encode(url.strip));
        @state = :CREATED;
        @data = "";
        @stats = {comments:     0,
                  start_tags:   0,
                  end_tags:     0,
                  empty_tags:   0,
                  links:        0,
                  words:        0,
                  unknown_tags: 0}
    end

    attr_accessor :url, :state, :data, :stats

    def push()
        case @state
            when :CREATED
                @state = :LOADING
            when :LOADING
                @state = :LOADED
            when :LOADED
                @state = :PROCESSING
            when :PROCESSING
                @state = :FINISHED
        end
    end

    def invalidate()
        @state = :INVALID
    end

    def <=>(op)
        return self.url <=> op.url
    end

    def to_s()
        "------------------------------------------------------------------\n" +
        "URL:          #{@url}\n"                                              +
        "Comments:     #{@stats[:comments]}\n"                                 +
        "Start-Tags:   #{@stats[:start_tags]}\n"                               +
        "End-Tags:     #{@stats[:end_tags]}\n"                                 +
        "Empty-Tags:   #{@stats[:empty_tags]}\n"                               +
        "Unknown-Tags: #{@stats[:unknown_tags]}\n"                             +
        "Links:        #{@stats[:links]}\n"                                    +
        "Words:        #{@stats[:words]}\n"                                    +
        "------------------------------------------------------------------"
    end
end

# Klasse zum Verwalten der besuchten URLs, der WorkerKraken-Objekte und aller
# HtmlData-Objekte
class WebCrawlerManager
    def initialize()
        @urls = Array.new()

        @dormant = Array.new()
        @active = Array.new()
        @finished = Array.new()

        @pool = Array.new(1){WebCrawler.new(self)}

        @totalJobs = 0

        puts "WebCrawler deployed!"
    end

    # Eine neue Seite zum Besuchen vormerken
    def crawlSite(url)
        if @totalJobs < 1000 and not @urls.include?(url)
            @totalJobs += 1
            @urls << url
            @dormant << HtmlData.new(url)
        end
    end

    # Den nächsten Job in der Queue von einem freien WebCrawler bearbeiten
    # lassen
    def doNextJob()
        return false if @dormant.empty?
        return false if @pool.empty?

        nextJob = @dormant.delete_at(0)
        nextCrawler = @pool.delete_at(0)

        @active << nextJob

        nextCrawler.doNextJob(nextJob)
    end

    # Einen Job nach dem Bearbeiten zurücklegen und den WebCrawler freigeben
    def queueJob(job, crawler)
        case job.state
            when :LOADED
                @dormant << job
            when :FINISHED
                @finished << job
            when :INVALID
                @totalJobs -= 1
        end

        @active.delete_if(){|x| x == job}
        @pool << crawler
    end

    def to_s()
        rslt = "#{@dormant.size} dormant jobs.\n" +
               "#{@active.size} active jobs.\n"   +
               "#{@finished.size} finished jobs."
        @finished.each do |job|
            rslt += "\n#{job}"
        end

        return rslt
    end
end

# Klasse zum Laden und Bearbeiten der HTML-Daten
class WebCrawler
    def initialize(wcm)
        @wcm = wcm
    end

    # Den nächsten Job bearbeiten
    def doNextJob(nextJob)
        nextJob.push()

        begin
            case nextJob.state
                when :LOADING
                    loadHtml(nextJob)
                when :PROCESSING
                    processHtml(nextJob)
            end
        rescue Errno::ECONNREFUSED
            nextJob.invalidate()
            puts "ERROR: Connection refused"
        rescue Errno::ECONNRESET
            nextJob.invalidate()
            puts "ERROR: Connection reset"
        rescue Errno::ETIMEDOUT
            nextJob.invalidate()
            puts "ERROR: Connection timed out"
        rescue URI::InvalidURIError
            nextJob.invalidate()
            puts "ERROR: Invalid URI"
        rescue SocketError
            nextJob.invalidate()
            puts "ERROR: SocketError"
        rescue Net::ReadTimeout
            nextJob.invalidate()
            puts "ERROR: Timeout"
        rescue Net::HTTPBadResponse
            nextJob.invalidate()
            puts "ERROR: Bad HTTP Response"
        end

        nextJob.push()
        @wcm.queueJob(nextJob, self)
    end

    # Die HTML-Daten für den Job laden
    def loadHtml(job)
        Net::HTTP.start(job.url.host, job.url.port) do |http|
            if(http.head(job.url.path).content_type == "text/html")
                puts "loadHtml(): http://" + job.url.host + job.url.path
                job.data = http.get(job.url.path).body
            else
                job.invalidate()
            end
        end
    end

    # Die HTML-Daten für den Job bearbeiten
    def processHtml(job)
        puts "processHtml(): http://" + job.url.host + job.url.path

        # Kommentare rausfiltern
        while (job.data =~ /<!--.*?-->/m)
            job.data = $`+ $'
            job.stats[:comments] += 1
        end

        # HTTP-Links rausfiltern (und zum weiterverfolgen melden)
        while (job.data =~ /<a href=["'](http:\/\/[^"']*?\/[^"']*?)["']>/)
            found_url = $1
            job.data = $`+ $'
            job.stats[:links] += 1
            job.stats[:start_tags] += 1
            @wcm.crawlSite(found_url)
        end

        # Anchor-Tags rausfiltern
        while (job.data =~ /<a href=.*?>.*?<\/a>/)
            job.data = $`+ $'
            job.stats[:links] += 1
            job.stats[:start_tags] += 1
            job.stats[:end_tags] += 1
        end

        # Start-Tags rausfiltern
        while (job.data =~ /<[^\/].*?[^\/]>/)
            job.data = $`+ $'
            job.stats[:start_tags] += 1
        end

        # End-Tags rausfiltern
        while (job.data =~ /<\/.*?>/)
            job.data = $`+ $'
            job.stats[:end_tags] += 1
        end

        # Empty-Tags rausfiltern
        while (job.data =~ /<.*?\/>/)
            job.data = $`+ $'
            job.stats[:empty_tags] += 1
        end

        # Restliche Tags rausfiltern
        while (job.data =~ /<.*?>/)
            job.data = $` + $'
            job.stats[:unknown_tags] += 1
        end

        # Wörter rausfiltern
        while (job.data =~ /\W\w+?\W/)
            job.data = $` + $'
            job.stats[:words] += 1
        end
    end
end

# Main-Loop des Crawlers
begin
    start_url = ARGV[0]
    cnt_sleep = 0
    wcm = WebCrawlerManager.new()
    wcm.crawlSite(start_url)

    begin
        if(!wcm.doNextJob())
            cnt_sleep += 1
            Kernel.sleep(1)
        else
            cnt_sleep = 0
        end
    end until(cnt_sleep >= 5) # Beenden, wenn 5 Sekunden lang kein Job bearbeitet wird
rescue Interrupt
    puts "\n"
ensure
    puts wcm
end
